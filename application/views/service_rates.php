<?php include('header.php');?>
		<div id="page-header">
			<h1>Service Rates</h1>
			<div class="title-block3"></div>
			<p><a href="#">Home</a><i class="fa fa-angle-right"></i>Service Rates</p>
		</div>
		
        <!-- BEGIN .content-wrapper-outer -->
       
		<div class="content-wrapper-outer clearfix">
        <div class="booking-step-wrapper clearfix">
					<div class="step-wrapper clearfix ">
						<div class="step-icon-wrapper">
							<div class="step-icon"><i class="fa fa-calendar"></i></div>
						</div>
                        <div class="step-title"> Departure</div>
						<div class="step-title"> 24-02-2018</div>
                        
					</div>
					<div class="step-wrapper clearfix" >
						<div class="step-icon-wrapper">
							<div class="step-icon step-icon-current"><i class="fa fa-calendar"></i></div>
						</div>
                        <div class="step-title">Return</div>
						<div class="step-title"> 28-02-2018</div>
                        
					</div>
					<div class="step-wrapper clearfix ">
						<div class="step-icon-wrapper">
							<div class="step-icon"><i class="fa fa-clock-o" style="font-size:25px;"></i></div>
						</div>
                        <div class="step-title">Time</div>
						<div class="step-title">9.00 PM</div>
                        
					</div>
                    <!-- <div class="step-line"></div> -->
                    <div class="row"></div>
                    <br>
            </div><hr>
			<!-- BEGIN .main-content -->
			<div class="main-content main-content-full">
				
				<!-- <div class="mobile-rate-table-msg msg default"><p>Mobile users, please swipe left/right to view prices</p></div> -->
				
				<!-- BEGIN .clearfix -->
				<div class="service-rate-table-wrapper clearfix">
					
					<!-- BEGIN .service-rate-table-inner-wrapper -->
					<div class="service-rate-table-inner-wrapper clearfix">
					
					<!-- BEGIN .car-list-wrapper -->
					<div class="car-list-wrapper">
						
						<!-- <div class="blank-header"></div> -->
						
						<!-- BEGIN .car-list-inner -->
						<div class="car-list-inner">
							
							<div class="car-list-section clearfix">
								<img src="../assets/images/image55.jpg" alt="" />
							</div><br>
							<div class="car-list-section clearfix">
								<img src="../assets/images/image55.jpg" alt="" />
                            </div><br>
                            <div class="car-list-section clearfix">
								<img src="../assets/images/image55.jpg" alt="" />
							</div>
							<!-- <div class="car-list-section clearfix">
								<img src="../assets/images/image56.jpg" alt="" /><p><strong>BMW Grand Sedan</strong></p>
							</div>
							
							<div class="car-list-section clearfix">
								<img src="../assets/images/image57.jpg" alt="" /><p><strong>Lincoln MKT</strong></p>
							</div>
							
							<div class="car-list-section clearfix">
								<img src="../assets/images/image58.jpg" alt="" /><p><strong>Audi Grand Sedan</strong></p>
							</div>
							
							<div class="car-list-section clearfix">
								<img src="../assets/images/image59.jpg" alt="" /><p><strong>Mercedes Sedan</strong></p>
							</div>
							
							<div class="car-list-section clearfix">
								<img src="../assets/images/image60.jpg" alt="" /><p><strong>Mercedes Van</strong></p>
							</div>
							
							<div class="car-list-section clearfix">
								<img src="../assets/images/image61.jpg" alt="" /><p><strong>Ford Party Bus</strong></p>
							</div>
							
							<div class="car-list-section clearfix">
								<img src="../assets/images/image62.jpg" alt="" /><p><strong>Mercedes Van</strong></p>
							</div> -->

						<!-- END .car-list-inner -->
						</div>
						
					<!-- END .car-list-wrapper -->
					</div>
					
					<!-- BEGIN .service-rate-wrapper -->
					<div class="service-rate-wrapper">
					
						<!-- <div class="service-rate-header"><p><strong>Hourly Rate</strong> Business Trips, Events</p></div> -->
                        <div class=""></div>
						<!-- BEGIN .service-rate-inner -->
						<div class="service-rate-inner">
							
							<div class="service-rate-section">
                            <!-- <p><strong>Cadillac Escalade</strong></p> -->
								<p><strong><span></span>Cadillac Escalade</strong>AC Mid-Size Plus</p>
							</div><br>
							<div class="service-rate-section">
                            <!-- <p><strong>Cadillac Escalade</strong></p> -->
								<p><strong><span></span>Cadillac Escalade</strong>AC Mid-Size Plus</p>
                            </div><br>
                            <div class="service-rate-section">
                            <!-- <p><strong>Cadillac Escalade</strong></p> -->
								<p><strong><span></span>Cadillac Escalade</strong>AC Mid-Size Plus</p>
							</div>
							<!-- <div class="service-rate-section">
								<p><strong><span>$150</span> / Hour</strong> + fuel and toll surcharges</p>
							</div>
							
							<div class="service-rate-section">
								<p><strong><span>$170</span> / Hour</strong> + fuel and toll surcharges</p>
							</div>
							
							<div class="service-rate-section">
								<p><strong><span>$190</span> / Hour</strong> + fuel and toll surcharges</p>
							</div>
							
							<div class="service-rate-section">
								<p><strong><span>$210</span> / Hour</strong> + fuel and toll surcharges</p>
							</div>
							
							<div class="service-rate-section">
								<p><strong><span>$230</span> / Hour</strong> + fuel and toll surcharges</p>
							</div>
							
							<div class="service-rate-section">
								<p><strong><span>$250</span> / Hour</strong> + fuel and toll surcharges</p>
							</div>
							
							<div class="service-rate-section">
								<p><strong><span>$270</span> / Hour</strong> + fuel and toll surcharges</p>
							</div> -->

						<!-- END .service-rate-inner -->
						</div>
					
					<!-- END .service-rate-wrapper -->
					</div>
					
					<!-- BEGIN .service-rate-wrapper -->
					<div class="service-rate-wrapper">
					
						<!-- <div class="service-rate-header"><p><strong>Daily Rate</strong> Weddings, Longer Trips</p></div> -->

						<!-- BEGIN .service-rate-inner -->
						<div class="service-rate-inner">
							
							<div class="service-rate-section">
								<p style="color:green;"><strong><span>₹  300</span></strong>Inclusive of GST</p>
							</div><br>
                            <div class="service-rate-section">
								<p style="color:green;"><strong><span>₹  300</span></strong>Inclusive of GST</p>
                            </div><br>
                            <div class="service-rate-section">
								<p style="color:green;"><strong><span>₹  300</span></strong>Inclusive of GST</p>
							</div>
							<!-- <div class="service-rate-section">
								<p><strong><span>$320</span> / Day</strong> + fuel and toll surcharges</p>
							</div>
							
							<div class="service-rate-section">
								<p><strong><span>$340</span> / Day</strong> + fuel and toll surcharges</p>
							</div>
							
							<div class="service-rate-section">
								<p><strong><span>$360</span> / Day</strong> + fuel and toll surcharges</p>
							</div>
							
							<div class="service-rate-section">
								<p><strong><span>$380</span> / Day</strong> + fuel and toll surcharges</p>
							</div>
							
							<div class="service-rate-section">
								<p><strong><span>$400</span> / Day</strong> + fuel and toll surcharges</p>
							</div>
							
							<div class="service-rate-section">
								<p><strong><span>$420</span> / Day</strong> + fuel and toll surcharges</p>
							</div>
							
							<div class="service-rate-section">
								<p><strong><span>$440</span> / Day</strong> + fuel and toll surcharges</p>
							</div> -->

						<!-- END .service-rate-inner -->
						</div>
					
					<!-- END .service-rate-wrapper -->
					</div>
					
					<!-- BEGIN .service-rate-wrapper -->
					<div class="service-rate-wrapper">
					
						<!-- o<div class="service-rate-header"><p><strong>Airprt Transfer</strong> Charged at a fixed price</p></div> -->

						<!-- BEGIN .service-rate-inner -->
						<div class="service-rate-inner">
							
							<div class="service-rate-section" style="height:46px;">
								<p><strong><button type="submit">SELECT</button></strong>Fare Details</p>
							</div><br>
                            <div class="service-rate-section" style="height:46px;">
								<p><strong><button type="submit">SELECT</button></strong>Fare Details</p>
                            </div><br>				
                            <div class="service-rate-section" style="height:46px;">
								<p><strong><button type="submit">SELECT</button></strong>Fare Details</p>
							</div>
							<!-- <div class="service-rate-section">
								<p><strong><span>$170</span></strong> + fuel and toll surcharges</p>
							</div>
							
							<div class="service-rate-section">
								<p><strong><span>$190</span></strong> + fuel and toll surcharges</p>
							</div>
							
							<div class="service-rate-section">
								<p><strong><span>$210</span></strong> + fuel and toll surcharges</p>
							</div>
							
							<div class="service-rate-section">
								<p><strong><span>$230</span></strong> + fuel and toll surcharges</p>
							</div>
							
							<div class="service-rate-section">
								<p><strong><span>$250</span></strong> + fuel and toll surcharges</p>
							</div>
							
							<div class="service-rate-section">
								<p><strong><span>$270</span></strong> + fuel and toll surcharges</p>
							</div>
							
							<div class="service-rate-section">
								<p><strong><span>$290</span></strong> + fuel and toll surcharges</p>
							</div> -->

						<!-- END .service-rate-inner -->
						</div>
					
					<!-- END .service-rate-wrapper -->
					</div>
					
					<!-- END .service-rate-table-inner-wrapper -->
					</div>
					
				<!-- END .clearfix -->
				</div>
				
				<!-- <div class="call-to-action-small clearfix">
					<h4>Place your booking online today with ease and enjoy your next trip in luxury</h4>
					<a href="#" class="call-to-action-button">Online Booking</a>
				</div> -->
				
			<!-- END .main-content -->
			</div>
		
		<!-- END .content-wrapper-outer -->
		</div>
	<?php include('footer.php');?>