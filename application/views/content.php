<div class="large-header-wrapper">

<!-- BEGIN .large-header -->
<div class="large-header">

    <!-- BEGIN .header-booking-form-wrapper -->
    <div class="header-booking-form-wrapper">

        <!-- BEGIN #booking-tabs -->
        <div id="booking-tabs">

            <ul class="nav clearfix">
                <li><a href="#tab-one-way">One-Way Transfer</a></li>
                <li><a href="#tab-hourly">Hourly Service</a></li>
            </ul>

            <!-- BEGIN #tab-one-way -->
            <div id="tab-one-way">

                <!-- BEGIN .booking-form-1 -->
                <form action="http://themes.quitenicestuff2.com/chauffeur/header-1/booking.html" class="booking-form-1" method="post">

                    <input type="text" name="pickup-address" value="" placeholder="Pick Up Address" />
                    <input type="text" name="dropoff-address" value="" placeholder="Drop Off Address" />
                    <input type="text" name="pickup-date" class="datepicker" value="" placeholder="Pick Up Date" />

                    <div class="booking-form-time">
                        <label>Pick Up Time</label>
                    </div>

                    <div class="booking-form-hour">
                        <div class="select-wrapper">
                            <i class="fa fa-angle-down"></i>
                            <select>
                                <option value="12am">12am</option>
                                <option value="01am">01am</option>
                                <option value="02am">02am</option>
                                <option value="03am">03am</option>
                                <option value="04am">04am</option>
                                <option value="05am">05am</option>
                                <option value="06am">06am</option>
                                <option value="07am">07am</option>
                                <option value="08am">08am</option>
                                <option value="09am">09am</option>
                                <option value="10am">10am</option>
                                <option value="11am">11am</option>
                                <option value="12pm">12pm</option>
                                <option value="01pm">01pm</option>
                                <option value="02pm">02pm</option>
                                <option value="03pm">03pm</option>
                                <option value="04pm">04pm</option>
                                <option value="05pm">05pm</option>
                                <option value="06pm">06pm</option>
                                <option value="07pm">07pm</option>
                                <option value="08pm">08pm</option>
                                <option value="09pm">09pm</option>
                                <option value="10pm">10pm</option>
                                <option value="11pm">11pm</option>
                            </select>
                        </div>
                    </div>

                    <div class="booking-form-min">
                        <div class="select-wrapper">
                            <i class="fa fa-angle-down"></i>
                            <select>
                                <option value="00">00</option>
                                <option value="15">15</option>
                                <option value="30">30</option>
                                <option value="45">45</option>
                            </select>
                        </div>
                    </div>

                    <button type="submit">
                         <span>Reserve Now</span>
                    </button>

                    <!-- END .booking-form-1 -->
                </form>

                <!-- END #tab-one-way -->
            </div>

            <!-- BEGIN #tab-hourly -->
            <div id="tab-hourly">

                <!-- BEGIN .booking-form-1 -->
                <form action="http://themes.quitenicestuff2.com/chauffeur/header-1/booking" class="booking-form-1" method="post">

                    <input type="text" name="pickup-address" value="" placeholder="Pick Up Address" />

                    <div class="one-third">
                        <label>Trip Duration</label>
                    </div>

                    <div class="two-thirds last-col">
                        <div class="select-wrapper">
                            <i class="fa fa-angle-down"></i>
                            <select>
                                <option value="1hr">1 Hour</option>
                                <option value="2hr">2 Hours</option>
                                <option value="3hr">3 Hours</option>
                                <option value="4hr">4 Hours</option>
                                <option value="5hr">5 Hours</option>
                                <option value="6hr">6 Hours</option>
                                <option value="7hr">7 Hours</option>
                                <option value="8hr">8 Hours</option>
                                <option value="9hr">9 Hours</option>
                                <option value="10hr">10 Hours</option>
                                <option value="11hr">11 Hours</option>
                                <option value="12hr">12 Hours</option>
                                <option value="13hr">13 Hours</option>
                                <option value="14hr">14 Hours</option>
                                <option value="15hr">15 Hours</option>
                                <option value="16hr">16 Hours</option>
                                <option value="17hr">17 Hours</option>
                                <option value="18hr">18 Hours</option>
                                <option value="19hr">19 Hours</option>
                                <option value="20hr">20 Hours</option>
                                <option value="21hr">21 Hours</option>
                                <option value="22hr">22 Hours</option>
                                <option value="23hr">23 Hours</option>
                                <option value="24hr">24 Hours</option>
                                <option value="25hr">25 Hours</option>
                                <option value="26hr">26 Hours</option>
                                <option value="27hr">27 Hours</option>
                                <option value="28hr">28 Hours</option>
                                <option value="29hr">29 Hours</option>
                                <option value="30hr">30 Hours</option>
                                <option value="31hr">31 Hours</option>
                                <option value="32hr">32 Hours</option>
                                <option value="33hr">33 Hours</option>
                                <option value="34hr">34 Hours</option>
                                <option value="35hr">35 Hours</option>
                                <option value="36hr">36 Hours</option>
                                <option value="37hr">37 Hours</option>
                                <option value="38hr">38 Hours</option>
                                <option value="39hr">39 Hours</option>
                                <option value="40hr">40 Hours</option>
                                <option value="41hr">41 Hours</option>
                                <option value="42hr">42 Hours</option>
                                <option value="43hr">43 Hours</option>
                                <option value="44hr">44 Hours</option>
                                <option value="45hr">45 Hours</option>
                                <option value="46hr">46 Hours</option>
                                <option value="47hr">47 Hours</option>
                                <option value="48hr">48 Hours</option>
                            </select>
                        </div>
                    </div>

                    <input type="text" name="dropoff-address" value="" placeholder="Drop Off Address" />
                    <input type="text" name="pickup-date" class="datepicker" value="" placeholder="Pick Up Date" />

                    <div class="booking-form-time">
                        <label>Pick Up Time</label>
                    </div>

                    <div class="booking-form-hour">
                        <div class="select-wrapper">
                            <i class="fa fa-angle-down"></i>
                            <select>
                                <option value="12am">12am</option>
                                <option value="01am">01am</option>
                                <option value="02am">02am</option>
                                <option value="03am">03am</option>
                                <option value="04am">04am</option>
                                <option value="05am">05am</option>
                                <option value="06am">06am</option>
                                <option value="07am">07am</option>
                                <option value="08am">08am</option>
                                <option value="09am">09am</option>
                                <option value="10am">10am</option>
                                <option value="11am">11am</option>
                                <option value="12pm">12pm</option>
                                <option value="01pm">01pm</option>
                                <option value="02pm">02pm</option>
                                <option value="03pm">03pm</option>
                                <option value="04pm">04pm</option>
                                <option value="05pm">05pm</option>
                                <option value="06pm">06pm</option>
                                <option value="07pm">07pm</option>
                                <option value="08pm">08pm</option>
                                <option value="09pm">09pm</option>
                                <option value="10pm">10pm</option>
                                <option value="11pm">11pm</option>
                            </select>
                        </div>
                    </div>

                    <div class="booking-form-min">
                        <div class="select-wrapper">
                            <i class="fa fa-angle-down"></i>
                            <select>
                                <option value="00">00</option>
                                <option value="15">15</option>
                                <option value="30">30</option>
                                <option value="45">45</option>
                            </select>
                        </div>
                    </div>

                    <button type="submit">
                         <span>Reserve Now</span>
                    </button>

                    <!-- END .booking-form-1 -->
                </form>

                <!-- END #tab-hourly -->
            </div>

            <!-- END #booking-tabs -->
        </div>

        <!-- END .header-booking-form-wrapper -->
    </div>

    <!-- END .large-header -->
</div>

<!-- END .large-header-wrapper -->
</div>
<section class="content-wrapper-outer content-wrapper clearfix our-fleet-sections">
    <h3 class="center-title">Our Fleet</h3>
    <div class="title-block2"></div>
    <p class="fleet-intro-text">Choose from a wide selection of boats ranging from luxury motor yachts to classic sailing yachts, we have every type of boat available to meet your needs. We also take custom orders and will help you acquire a specific yacht.</p>
</section>

<section class="content-wrapper-outer home-icons-outer-wrapper-2">

<!-- BEGIN .clearfix -->
<div class="clearfix">

    <!-- BEGIN .qns-one-third -->
    <div class="qns-one-half home-icon-wrapper-2">
        <div class="qns-home-icon"><i class="fa fa-calendar-check-o"></i></div>
        <div class="home-icon-inner">
            <h4>Easy Online Booking</h4>
            <div class="title-block3"></div>
            <p>Vestibulum efficitur se sit amet sem semper luctus pellentes auctor tristique ornare. Ut porta ut magna in cursus.</p>
        </div>
        <!-- END .qns-one-third -->
    </div>

    <!-- BEGIN .qns-one-third -->
    <div class="qns-one-half home-icon-wrapper-2 qns-last">
        <div class="qns-home-icon"><i class="fa fa-star"></i></div>
        <div class="home-icon-inner">
            <h4>Professional Drivers</h4>
            <div class="title-block3"></div>
            <p>Vestibulum efficitur se sit amet sem semper luctus pellentes auctor tristique ornare. Ut porta ut magna in cursus.</p>
        </div>
        <!-- END .qns-one-third -->
    </div>

    <!-- BEGIN .qns-one-third -->
    <div class="qns-one-half home-icon-wrapper-2">
        <div class="qns-home-icon"><i class="fa fa-car"></i></div>
        <div class="home-icon-inner">
            <h4>Big Fleet Of Vehicles</h4>
            <div class="title-block3"></div>
            <p>Vestibulum efficitur se sit amet sem semper luctus pellentes auctor tristique ornare. Ut porta ut magna in cursus.</p>
        </div>
        <!-- END .qns-one-third -->
    </div>

    <!-- BEGIN .qns-one-third -->
    <div class="qns-one-half home-icon-wrapper-2 qns-last">
        <div class="qns-home-icon"><i class="fa fa-cc-visa"></i></div>
        <div class="home-icon-inner">
            <h4>Online Payment</h4>
            <div class="title-block3"></div>
            <p>Vestibulum efficitur se sit amet sem semper luctus pellentes auctor tristique ornare. Ut porta ut magna in cursus.</p>
        </div>
        <!-- END .qns-one-third -->
    </div>

    <!-- END .clearfix -->
</div>

<!-- END .content-wrapper-outer -->
</section>

<!-- BEGIN .clearfix -->
<section class="clearfix">



<!-- END .clearfix -->
</section>

<!-- BEGIN .content-wrapper-outer -->
<section class="content-wrapper-outer content-wrapper clearfix latest-news-section">

<h3 class="center-title">Latest News</h3>
<div class="title-block2"></div>

<!-- BEGIN .latest-news-block-wrapper -->
<div class="owl-carousel2 latest-news-block-wrapper">

    <!-- BEGIN .latest-news-block -->
    <div class="latest-news-block">

        <div class="latest-news-block-image">
            <a href="news-single.html"><img src="<?php echo base_url();?>assets-front/images/image7.jpg" alt="" /></a>
        </div>

        <div class="latest-news-block-content">

            <h4><a href="news-single.html">This Week Only 10% Discounts On Fridays</a></h4>

            <!-- BEGIN .news-meta -->
            <div class="news-meta clearfix">
                <span class="nm-news-date">19th January 2017</span>
                <span class="nm-news-comments">2 Comments</span>
                <!-- END .news-meta -->
            </div>

            <div class="latest-news-excerpt">
                <p>Sed suscipit pretium venenatis. Praesent ultrices convallis augue, in tincidunt nibh hendrerit sit amet. </p>
            </div>

        </div>

        <!-- END .latest-news-block -->
    </div>

    <!-- BEGIN .latest-news-block -->
    <div class="latest-news-block">

        <div class="latest-news-block-image">
            <a href="news-single.html"><img src="<?php echo base_url();?>assets-front/images/image8.jpg" alt="" /></a>
        </div>

        <div class="latest-news-block-content">

            <h4><a href="news-single.html">We Just Took Delivery Of Our New Vehicles!</a></h4>

            <!-- BEGIN .news-meta -->
            <div class="news-meta clearfix">
                <span class="nm-news-date">15th January 2017</span>
                <span class="nm-news-comments">3 Comments</span>
                <!-- END .news-meta -->
            </div>

            <div class="latest-news-excerpt">
                <p>Sed suscipit pretium venenatis. Praesent ultrices convallis augue, in tincidunt nibh hendrerit sit amet. </p>
            </div>

        </div>

        <!-- END .latest-news-block -->
    </div>

    <!-- BEGIN .latest-news-block -->
    <div class="latest-news-block">

        <div class="latest-news-block-image">
            <a href="news-single.html"><img src="<?php echo base_url();?>assets-front/images/image9.jpg" alt="" /></a>
        </div>

        <div class="latest-news-block-content">

            <h4><a href="news-single.html">New Safety Procedures Now In Place</a></h4>

            <!-- BEGIN .news-meta -->
            <div class="news-meta clearfix">
                <span class="nm-news-date">6th January 2017</span>
                <span class="nm-news-comments">1 Comments</span>
                <!-- END .news-meta -->
            </div>

            <div class="latest-news-excerpt">
                <p>Sed suscipit pretium venenatis. Praesent ultrices convallis augue, in tincidunt nibh hendrerit sit amet. </p>
            </div>

        </div>

        <!-- END .latest-news-block -->
    </div>

    <!-- BEGIN .latest-news-block -->
    <div class="latest-news-block">

        <div class="latest-news-block-image">
            <a href="news-single.html"><img src="<?php echo base_url();?>assets-front/images/image7.jpg" alt="" /></a>
        </div>

        <div class="latest-news-block-content">

            <h4><a href="news-single.html">This Week Only 10% Discounts On Fridays</a></h4>

            <!-- BEGIN .news-meta -->
            <div class="news-meta clearfix">
                <span class="nm-news-date">19th January 2017</span>
                <span class="nm-news-comments">2 Comments</span>
                <!-- END .news-meta -->
            </div>

            <div class="latest-news-excerpt">
                <p>Sed suscipit pretium venenatis. Praesent ultrices convallis augue, in tincidunt nibh hendrerit sit amet. </p>
            </div>

        </div>

        <!-- END .latest-news-block -->
    </div>

    <!-- BEGIN .latest-news-block -->
    <div class="latest-news-block">

        <div class="latest-news-block-image">
            <a href="news-single.html"><img src="<?php echo base_url();?>assets-front/images/image8.jpg" alt="" /></a>
        </div>

        <div class="latest-news-block-content">

            <h4><a href="news-single.html">We Just Took Delivery Of Our New Vehicles!</a></h4>

            <!-- BEGIN .news-meta -->
            <div class="news-meta clearfix">
                <span class="nm-news-date">15th January 2017</span>
                <span class="nm-news-comments">3 Comments</span>
                <!-- END .news-meta -->
            </div>

            <div class="latest-news-excerpt">
                <p>Sed suscipit pretium venenatis. Praesent ultrices convallis augue, in tincidunt nibh hendrerit sit amet. </p>
            </div>

        </div>

        <!-- END .latest-news-block -->
    </div>

    <!-- BEGIN .latest-news-block -->
    <div class="latest-news-block">

        <div class="latest-news-block-image">
            <a href="news-single.html"><img src="<?php echo base_url();?>assets-front/images/image9.jpg" alt="" /></a>
        </div>

        <div class="latest-news-block-content">

            <h4><a href="news-single.html">New Safety Procedures Now In Place</a></h4>

            <!-- BEGIN .news-meta -->
            <div class="news-meta clearfix">
                <span class="nm-news-date">6th January 2017</span>
                <span class="nm-news-comments">1 Comments</span>
                <!-- END .news-meta -->
            </div>

            <div class="latest-news-excerpt">
                <p>Sed suscipit pretium venenatis. Praesent ultrices convallis augue, in tincidunt nibh hendrerit sit amet. </p>
            </div>

        </div>

        <!-- END .latest-news-block -->
    </div>

    <!-- BEGIN .latest-news-block -->
    <div class="latest-news-block">

        <div class="latest-news-block-image">
            <a href="news-single.html"><img src="<?php echo base_url();?>assets-front/images/image7.jpg" alt="" /></a>
        </div>

        <div class="latest-news-block-content">

            <h4><a href="news-single.html">This Week Only 10% Discounts On Fridays</a></h4>

            <!-- BEGIN .news-meta -->
            <div class="news-meta clearfix">
                <span class="nm-news-date">19th January 2017</span>
                <span class="nm-news-comments">2 Comments</span>
                <!-- END .news-meta -->
            </div>

            <div class="latest-news-excerpt">
                <p>Sed suscipit pretium venenatis. Praesent ultrices convallis augue, in tincidunt nibh hendrerit sit amet. </p>
            </div>

        </div>

        <!-- END .latest-news-block -->
    </div>

    <!-- END .latest-news-block-wrapper -->
</div>

<!-- END .content-wrapper-outer -->
</section>

<!-- BEGIN .call-to-action-2-section -->
<section class="clearfix call-to-action-2-section">

<div class="call-to-action-2-section-inner">

    <h3>Book Online Today And Travel In Comfort On Your Next Trip</h3>
    <div class="title-block5"></div>
    <p>Call Us On 1800-1111-2222 or Email booking@website.com</p>
    <!-- <a href="booking.html" class="button0">Online Booking</a> -->

</div>

<!-- END .call-to-action-2-section -->
</section>

<!-- BEGIN .content-wrapper-outer -->
<section class="content-wrapper-outer content-wrapper clearfix our-fleet-sections">
<div class="title-block2"></div>
<br>
<div class="owl-carousel1 fleet-block-wrapper">

    <!-- BEGIN .fleet-block -->
    <div class="fleet-block">
        <div class="fleet-block-image">
            <a href="our-fleet-single.html"><img src="<?php echo base_url();?>assets-front/images/image1.jpg" alt="" /></a>
        </div>
        <div class="fleet-block-content">
            <div class="fleet-price">From $110</div>
            <h4><a href="fleet-charter-single.html">BMW Grand Sedan</a></h4>
            <div class="title-block3"></div>
            <ul class="list-style4">
                <li>4 Passengers</li>
                <li>2 Bags</li>
            </ul>
        </div>
        <!-- END .fleet-block -->
    </div>

    <!-- BEGIN .fleet-block -->
    <div class="fleet-block">
        <div class="fleet-block-image">
            <a href="our-fleet-single.html"><img src="<?php echo base_url();?>assets-front/images/image2.jpg" alt="" /></a>
        </div>
        <div class="fleet-block-content">
            <div class="fleet-price">From $150</div>
            <h4><a href="fleet-charter-single.html">Cadillac Escalade</a></h4>
            <div class="title-block3"></div>
            <ul class="list-style4">
                <li>6 Passengers</li>
                <li>4 Bags</li>
            </ul>
        </div>
        <!-- END .fleet-block -->
    </div>

    <!-- BEGIN .fleet-block -->
    <div class="fleet-block">
        <div class="fleet-block-image">
            <a href="our-fleet-single.html"><img src="<?php echo base_url();?>assets-front/images/image3.jpg" alt="" /></a>
        </div>
        <div class="fleet-block-content">
            <div class="fleet-price">From $90</div>
            <h4><a href="fleet-charter-single.html">Lincoln MKT</a></h4>
            <div class="title-block3"></div>
            <ul class="list-style4">
                <li>6 Passengers</li>
                <li>3 Bags</li>
            </ul>
        </div>
        <!-- END .fleet-block -->
    </div>

    <!-- BEGIN .fleet-block -->
    <div class="fleet-block">
        <div class="fleet-block-image">
            <a href="our-fleet-single.html"><img src="<?php echo base_url();?>assets-front/images/image4.jpg" alt="" /></a>
        </div>
        <div class="fleet-block-content">
            <div class="fleet-price">From $130</div>
            <h4><a href="fleet-charter-single.html">Audi Grand Sedan</a></h4>
            <div class="title-block3"></div>
            <ul class="list-style4">
                <li>4 Passengers</li>
                <li>2 Bags</li>
            </ul>
        </div>
        <!-- END .fleet-block -->
    </div>

    <!-- BEGIN .fleet-block -->
    <div class="fleet-block">
        <div class="fleet-block-image">
            <a href="our-fleet-single.html"><img src="<?php echo base_url();?>assets-front/images/image18.jpg" alt="" /></a>
        </div>
        <div class="fleet-block-content">
            <div class="fleet-price">From $140</div>
            <h4><a href="fleet-charter-single.html">Mercedes Sedan</a></h4>
            <div class="title-block3"></div>
            <ul class="list-style4">
                <li>4 Passengers</li>
                <li>2 Bags</li>
            </ul>
        </div>
        <!-- END .fleet-block -->
    </div>

    <!-- BEGIN .fleet-block -->
    <div class="fleet-block">
        <div class="fleet-block-image">
            <a href="our-fleet-single.html"><img src="<?php echo base_url();?>assets-front/images/image19.jpg" alt="" /></a>
        </div>
        <div class="fleet-block-content">
            <div class="fleet-price">From $190</div>
            <h4><a href="fleet-charter-single.html">Mercedes Van</a></h4>
            <div class="title-block3"></div>
            <ul class="list-style4">
                <li>6 Passengers</li>
                <li>4 Bags</li>
            </ul>
        </div>
        <!-- END .fleet-block -->
    </div>

    <!-- BEGIN .fleet-block -->
    <div class="fleet-block">
        <div class="fleet-block-image">
            <a href="our-fleet-single.html"><img src="<?php echo base_url();?>assets-front/images/image20.jpg" alt="" /></a>
        </div>
        <div class="fleet-block-content">
            <div class="fleet-price">From $240</div>
            <h4><a href="fleet-charter-single.html">Ford Party Bus</a></h4>
            <div class="title-block3"></div>
            <ul class="list-style4">
                <li>8 Passengers</li>
                <li>5 Bags</li>
            </ul>
        </div>
        <!-- END .fleet-block -->
    </div>

    <!-- BEGIN .fleet-block -->
    <div class="fleet-block">
        <div class="fleet-block-image">
            <a href="our-fleet-single.html"><img src="<?php echo base_url();?>assets-front/images/image22.jpg" alt="" /></a>
        </div>
        <div class="fleet-block-content">
            <div class="fleet-price">From $140</div>
            <h4><a href="fleet-charter-single.html">Mercedes Van</a></h4>
            <div class="title-block3"></div>
            <ul class="list-style4">
                <li>6 Passengers</li>
                <li>3 Bags</li>
            </ul>
        </div>
        <!-- END .fleet-block -->
    </div>

    <!-- BEGIN .fleet-block -->
    <div class="fleet-block">
        <div class="fleet-block-image">
            <a href="our-fleet-single.html"><img src="<?php echo base_url();?>assets-front/images/image1.jpg" alt="" /></a>
        </div>
        <div class="fleet-block-content">
            <div class="fleet-price">From $110</div>
            <h4><a href="fleet-charter-single.html">BMW Grand Sedan</a></h4>
            <div class="title-block3"></div>
            <ul class="list-style4">
                <li>4 Passengers</li>
                <li>2 Bags</li>
            </ul>
        </div>
        <!-- END .fleet-block -->
    </div>

    <!-- END .fleet-block-wrapper -->
</div>
</section>
