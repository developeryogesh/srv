<!DOCTYPE html>
<!--[if lt IE 7]> <html dir="ltr" lang="en-US" class="ie6"> <![endif]-->
<!--[if IE 7]>    <html dir="ltr" lang="en-US" class="ie7"> <![endif]-->
<!--[if IE 8]>    <html dir="ltr" lang="en-US" class="ie8"> <![endif]-->
<!--[if gt IE 8]><!-->
<html dir="ltr" lang="en-US">
<!--<![endif]-->

<!-- BEGIN head -->

<!-- Mirrored from themes.quitenicestuff2.com/chauffeur/header-1/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 26 Apr 2018 07:17:00 GMT -->

<head>

    <!--Meta Tags-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <!-- Title -->
    <title>Savvari.com</title>

    <!-- JavaScript (must go in header) -->

    <script type="text/javascript" src="<?php echo base_url();?>assets-front/js/fontawesome-markers.min.js"></script>

    <!-- Stylesheets -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets-front/css/style.css" type="text/css" media="all" />

    <link rel="stylesheet" href="<?php echo base_url();?>assets-front/css/color-red.css" type="text/css" media="all" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets-front/css/responsive.css" type="text/css" media="all" />
    <link href="<?php echo base_url();?>assets-front/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets-front/css/settings.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets-front/css/layers.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets-front/css/navigation.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets-front/css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets-front/css/prettyPhoto.css">
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
	<script src="<?php echo base_url();?>assets-front/js/jquery.min.js"></script>

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.html" type="image/x-icon" />

    <!-- END head -->
</head>

<!-- BEGIN body -->

<body>

    <!-- BEGIN DEMO ONLY -->
    <!-- <div class="content-wrapper" id="style_picker">

        <h3 class="picker_title">Pick a Colour</h3>

        <ul class="style_list">
            <li><a href="http://themes.quitenicestuff2.com/chauffeur/style.php?c=red">Red</a></li>
            <li><a href="http://themes.quitenicestuff2.com/chauffeur/style.php?c=blue">Blue</a></li>
            <li><a href="http://themes.quitenicestuff2.com/chauffeur/style.php?c=orange">Orange</a></li>
            <li><a href="http://themes.quitenicestuff2.com/chauffeur/style.php?c=yellow">Yellow</a></li>
            <li><a href="http://themes.quitenicestuff2.com/chauffeur/style.php?c=green">Green</a></li>
        </ul>

        <div class="style_picker_toggle_wrapper">
            <a href="#" class="style_picker_toggle"><i class="fa fa-cog" aria-hidden="true"></i></a>
        </div>

    </div> -->
    <!-- END DEMO ONLY -->

    <!-- BEGIN .outer-wrapper -->
    <div class="outer-wrapper">

        <!-- BEGIN .header-area-1 -->
        <div class="header-area-1">

            <!-- BEGIN .top-bar-wrapper -->
          
            <!-- BEGIN .header-content -->
            <div class="header-content">

                <!-- BEGIN .logo -->
                <div class="logo">
                    <h2><a href="<?php echo base_url('client/homecontroller');?>">Savaari.com</a></h2>
                    <!-- END .logo -->
                </div>

                <!-- BEGIN .header-icons-wrapper -->
                <div class="header-icons-wrapper clearfix">

                    <a href="tel:0 90 4545 0000" class="topright-button"><span>96 0116 8678</span></a>

                    <!-- BEGIN .header-icons-inner -->
                    <div class="header-icons-inner clearfix">

                        <!-- BEGIN .header-icon -->
                        <div class="header-icon">
                            <p><i class="fa fa-check-square-o" aria-hidden="true"></i><strong>Airport Transfers</strong></p>
                            <p class="header-icon-text">Offered At Short Notice</p>
                            <!-- END .header-icon -->
                        </div>

                        <!-- BEGIN .header-icon -->
                        <div class="header-icon">
                            <p><i class="fa fa-check-square-o" aria-hidden="true"></i><strong>Wedding Parties</strong></p>
                            <p class="header-icon-text">Can Be Easily Arranged</p>
                            <!-- END .header-icon -->
                        </div>

                        <!-- BEGIN .header-icon -->
                        <div class="header-icon">
                            <p><i class="fa fa-check-square-o" aria-hidden="true"></i><strong>Business Meetings</strong></p>
                            <p class="header-icon-text">You Will Arrive On Time</p>
                            <!-- END .header-icon -->
                        </div>

                        <!-- END .header-icons-inner -->
                    </div>

                    <!-- END .header-icons-wrapper -->
                </div>

                <!-- <div id="mobile-navigation">
                    <a href="#search-lightbox" data-gal="prettyPhoto"><i class="fa fa-search"></i></a>
                    <a href="#" id="mobile-navigation-btn"><i class="fa fa-bars"></i></a>
                </div> -->

                <div class="clearboth"></div>

                <!-- BEGIN .mobile-navigation-wrapper -->
                <div class="mobile-navigation-wrapper">

                    <ul>
                        <li class="current-menu-item current_page_item menu-item-has-children"><a href="index.html">Home</a>
                            <!-- <ul>
                                <li><a href="index.html">Home I</a></li>
                                <li><a href="index2.html">Home II</a></li>
                            </ul> -->
                        </li>
                        <!-- <li class="megamenu-3-col menu-item-has-children">
                            <a href="our-fleet-3-col.html">Our Fleet</a>
                            <ul>
                                <li><a href="our-fleet-3-col.html">Our Fleet (Sidebar)</a>
                                    <ul>
                                        <li><a href="our-fleet-2-col.html">2 Columns</a></li>
                                        <li><a href="our-fleet-3-col.html">3 Columns</a></li>
                                        <li><a href="our-fleet-4-col.html">4 Columns</a></li>
                                        <li><a href="our-fleet-5-col.html">5 Columns</a></li>
                                    </ul>
                                </li>
                                <li><a href="our-fleet-3-col-full.html">Our Fleet (Full Width)</a>
                                    <ul>
                                        <li><a href="our-fleet-2-col-full.html">2 Columns</a></li>
                                        <li><a href="our-fleet-3-col-full.html">3 Columns</a></li>
                                        <li><a href="our-fleet-4-col-full.html">4 Columns</a></li>
                                        <li><a href="our-fleet-5-col-full.html">5 Columns</a></li>
                                    </ul>
                                </li>
                                <li><a href="our-fleet-single.html">Our Fleet Single</a>
                                    <ul>
                                        <li><a href="our-fleet-single.html">Our Fleet Single</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li> -->
                        <!-- <li class="menu-item-has-children">
                            <a href="service-rates.html">Service Rates</a>
                            <ul>
                                <li><a href="service-rates.html">Service Rates</a></li>
                                <li><a href="booking.html">Booking Step 1</a></li>
                                <li><a href="booking2.html">Booking Step 2</a></li>
                                <li><a href="booking3.html">Booking Step 3</a></li>
                                <li><a href="booking4.html">Booking Step 4</a></li>
                            </ul>
                        </li> -->
                        <li class="megamenu-3-col menu-item-has-children">
                            <a href="pages.html">Pages</a>
                            <ul>
                                <li><a href="pages.html">Pages</a>
                                    <ul>
                                        <li><a href="booking.html">Booking</a></li>
                                        <li><a href="typography.html">Typography</a></li>
                                        <li><a href="404-page.html">404 Page</a></li>
                                        <li><a href="search-results.html">Search Results</a></li>
                                        <li><a href="js-elements.html">JS Elements</a></li>
                                        <li><a href="full-width.html">Full Width</a></li>
                                    </ul>
                                </li>
                                <li><a href="photos-4-col.html">Photos (Sidebar)</a>
                                    <ul>
                                        <li><a href="photos-2-col.html">2 Columns</a></li>
                                        <li><a href="photos-3-col.html">3 Columns</a></li>
                                        <li><a href="photos-4-col.html">4 Columns</a></li>
                                        <li><a href="photos-5-col.html">5 Columns</a></li>
                                    </ul>
                                </li>
                                <li><a href="photos-4-col-full.html">Photos (Full Width)</a>
                                    <ul>
                                        <li><a href="photos-2-col-full.html">2 Columns</a></li>
                                        <li><a href="photos-3-col-full.html">3 Columns</a></li>
                                        <li><a href="photos-4-col-full.html">4 Columns</a></li>
                                        <li><a href="photos-5-col-full.html">5 Columns</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="megamenu-3-col menu-item-has-children">
                            <a href="news-3-col-grid.html">News</a>
                            <ul>
                                <li><a href="news-3-col-grid.html">News (Grid)</a>
                                    <ul>
                                        <li><a href="news-2-col-grid.html">2 Columns</a></li>
                                        <li><a href="news-3-col-grid.html">3 Columns</a></li>
                                        <li><a href="news-4-col-grid.html">4 Columns</a></li>
                                        <li><a href="news-5-col-grid.html">5 Columns</a></li>
                                    </ul>
                                </li>
                                <li><a href="news-1-col-listing.html">News (Listing)</a>
                                    <ul>
                                        <li><a href="news-1-col-listing.html">News (Listing)</a></li>
                                    </ul>
                                </li>
                                <li><a href="news-single.html">News Single Page</a>
                                    <ul>
                                        <li><a href="news-single.html">News Single Page</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="menu-item-has-children"><a href="testimonials.html">Testimonials</a>
                            <ul>
                                <li><a href="testimonials.html">Testimonials (Right Sidebar)</a></li>
                                <li><a href="testimonials-full.html">Testimonials (Full Width)</a></li>
                                <li><a href="testimonials-single.html">Testimonials Single</a></li>
                            </ul>
                        </li>
                        <li class="menu-item-has-children"><a href="<?php echo base_url('client/aboutuscontroller');?>">About Us</a>
                            <!-- <ul>
                                <li><a href="about-us.html">About I</a></li>
                                <li><a href="about-us2.html">About II</a></li>
                            </ul> -->
                        </li>
                        <li class="menu-item-has-children"><a href="contact-us.html">Contact Us</a></li>
                        <li class="menu-item-has-children"><a href="<?php echo base_url('client/logincontroller');?>">Login</a></li>
                        <li class="menu-item-has-children"><a href="<?php echo base_url('client/registercontroller');?>">Register</a></li>
                        <li class="menu-item-has-children"><a href="contact-us.html">Send Enquiry</a></li>                 
                    </ul>

                    <!-- END .mobile-navigation-wrapper -->
                </div>

                <!-- END .header-content -->
            </div>

            <!-- BEGIN #primary-navigation -->
            <nav id="primary-navigation" class="navigation-wrapper fixed-navigation clearfix">

                <!-- BEGIN .navigation-inner -->
                <div class="navigation-inner">

                    <!-- BEGIN .navigation -->
                    <div class="navigation">

                        <ul>
                            <li class="current-menu-item current_page_item menu-item-has-children"><a href="<?php echo base_url('client/homecontroller');?>">Home</a>
                                <!-- <ul>
                                    <li><a href="index.html">Home I</a></li>
                                    <li><a href="index2.html">Home II</a></li>
                                </ul> -->
                            </li>
                            <!-- <li class="megamenu-3-col menu-item-has-children">
                                <a href="our-fleet-3-col.html">Our Fleet</a>
                                <ul>
                                    <li><a href="our-fleet-3-col.html">Our Fleet (Sidebar)</a>
                                        <ul>
                                            <li><a href="our-fleet-2-col.html">2 Columns</a></li>
                                            <li><a href="our-fleet-3-col.html">3 Columns</a></li>
                                            <li><a href="our-fleet-4-col.html">4 Columns</a></li>
                                            <li><a href="our-fleet-5-col.html">5 Columns</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="our-fleet-3-col-full.html">Our Fleet (Full Width)</a>
                                        <ul>
                                            <li><a href="our-fleet-2-col-full.html">2 Columns</a></li>
                                            <li><a href="our-fleet-3-col-full.html">3 Columns</a></li>
                                            <li><a href="our-fleet-4-col-full.html">4 Columns</a></li>
                                            <li><a href="our-fleet-5-col-full.html">5 Columns</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="our-fleet-single.html">Our Fleet Single</a>
                                        <ul>
                                            <li><a href="our-fleet-single.html">Our Fleet Single</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li> -->
                            <!-- <li class="menu-item-has-children">
                                <a href="service-rates.html">Service Rates</a>
                                <ul>
                                    <li><a href="service-rates.html">Service Rates</a></li>
                                    <li><a href="booking.html">Booking Step 1</a></li>
                                    <li><a href="booking2.html">Booking Step 2</a></li>
                                    <li><a href="booking3.html">Booking Step 3</a></li>
                                    <li><a href="booking4.html">Booking Step 4</a></li>
                                </ul>
                            </li> -->
                            <!-- <li class="megamenu-3-col menu-item-has-children">
                                <a href="pages.html">Pages</a>
                                <ul>
                                    <li><a href="pages.html">Pages</a>
                                        <ul>
                                            <li><a href="booking.html">Booking</a></li>
                                            <li><a href="typography.html">Typography</a></li>
                                            <li><a href="404-page.html">404 Page</a></li>
                                            <li><a href="search-results.html">Search Results</a></li>
                                            <li><a href="js-elements.html">JS Elements</a></li>
                                            <li><a href="full-width.html">Full Width</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="photos-4-col.html">Photos (Sidebar)</a>
                                        <ul>
                                            <li><a href="photos-2-col.html">2 Columns</a></li>
                                            <li><a href="photos-3-col.html">3 Columns</a></li>
                                            <li><a href="photos-4-col.html">4 Columns</a></li>
                                            <li><a href="photos-5-col.html">5 Columns</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="photos-4-col-full.html">Photos (Full Width)</a>
                                        <ul>
                                            <li><a href="photos-2-col-full.html">2 Columns</a></li>
                                            <li><a href="photos-3-col-full.html">3 Columns</a></li>
                                            <li><a href="photos-4-col-full.html">4 Columns</a></li>
                                            <li><a href="photos-5-col-full.html">5 Columns</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li> -->
                            <!-- <li class="megamenu-3-col menu-item-has-children">
                                <a href="news-3-col-grid.html">News</a>
                                <ul>
                                    <li><a href="news-3-col-grid.html">News (Grid)</a>
                                        <ul>
                                            <li><a href="news-2-col-grid.html">2 Columns</a></li>
                                            <li><a href="news-3-col-grid.html">3 Columns</a></li>
                                            <li><a href="news-4-col-grid.html">4 Columns</a></li>
                                            <li><a href="news-5-col-grid.html">5 Columns</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="news-1-col-listing.html">News (Listing)</a>
                                        <ul>
                                            <li><a href="news-1-col-listing.html">News (Listing)</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="news-single.html">News Single Page</a>
                                        <ul>
                                            <li><a href="news-single.html">News Single Page</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li> -->
                            <!-- <li class="menu-item-has-children"><a href="testimonials.html">Testimonials</a>
                                <ul>
                                    <li><a href="testimonials.html">Testimonials (Right Sidebar)</a></li>
                                    <li><a href="testimonials-full.html">Testimonials (Full Width)</a></li>
                                    <li><a href="testimonials-single.html">Testimonials Single</a></li>
                                </ul>
                            </li> -->
                            <li class="menu-item-has-children"><a href="<?php echo base_url('client/aboutuscontroller');?>">About Us</a>
                                <!-- <ul>
                                    <li><a href="about-us.html">About I</a></li>
                                    <li><a href="about-us2.html">About II</a></li>
                                </ul> -->
                            </li>
                            <li class="menu-item-has-children"><a href="<?php echo base_url('client/aboutuscontroller/contact');?>">Contact Us</a>
                                <!-- <ul>
                                    <li><a href="contact-us.html">Contact I</a></li>
                                    <li><a href="contact-us2.html">Contact II</a></li>
                                </ul> -->
                            </li>
                            <li class="menu-item-has-children"><a href="<?php echo base_url('client/logincontroller');?>">Login</a></li>
                        <li class="menu-item-has-children"><a href="<?php echo base_url('client/registercontroller');?>">Register</a></li>
                        <li class="menu-item-has-children"><a href="<?php echo base_url('client/logincontroller/enquiry');?>">Send Enquiry</a></li>  
                        </ul>

                        <!-- END .navigation -->
                    </div>

                    <!-- <a href="#search-lightbox" data-gal="prettyPhoto"><i class="fa fa-search"></i></a> -->

                    <!-- BEGIN #search-lightbox -->
                    <div id="search-lightbox">

                        <!-- BEGIN .search-lightbox-inner -->
                        <div class="search-lightbox-inner">

                            <form name="s" action="http://themes.quitenicestuff2.com/chauffeur/header-1/search-results.html" method="post">
                                <input class="menu-search-field" type="text" onblur="if(this.value=='')this.value='To search, type and hit enter';" onfocus="if(this.value=='To search, type and hit enter')this.value='';" value="To search, type and hit enter" name="s" />
                            </form>

                            <!-- END .search-lightbox-inner -->
                        </div>

                        <!-- END #search-lightbox -->
                    </div>

                    <!-- END .navigation-inner -->
                </div>

                <!-- END #primary-navigation -->
            </nav>

            <!-- END .header-area-1 -->
        </div>

        <!-- BEGIN .large-header-wrapper -->
      
