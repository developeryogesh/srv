		<!-- BEGIN .footer -->
		<footer class="footer">

			<!-- BEGIN .footer-inner -->
			<div class="footer-inner clearfix">

				<!-- BEGIN .one-half -->
				<div class="one-half">

					<h5>Quick Link</h5>
					<div class="title-block6"></div>
					<ul class="pad0">
						<li href="#">About Us</li>
						<li href="#">Contact Us</li>
						<li href="#">Register</li>
						<li href="#">Login</li>
						<li href="#">Send Enquiry</li>
					</ul>
					<!-- <p>Pellentesque feugiat feugiat ipsum, nec sollicitudin tortor convallis et. In venenatis, turpis eu condimentum ultrices, dui erat elementum ante, vitae finibus nisi libero a justo. Mauris ex ante, imperdiet et elementum id, mollis.</p>

					<p>Quis felis. Praesent aliquet porttitor sem, at pretium erat tristique at. Proin molestie eros et ipsum pretium efficitur. Curabitur ligula sem, auctor at malesuada non, lobortis ornare dolor. Ut vitae ante non lectus fermentum</p> -->

				<!-- END .one-half -->
				</div>

				<!-- BEGIN .one-fourth -->
				<div class="one-fourth">

					<h5>Newsletter</h5>
					<div class="title-block6"></div>
					<p>Subscribe to our newsletter for news, updates, exclusive discounts and offers.</p>

					<!-- BEGIN .newsletter-form -->
					<form action="#" class="newsletter-form" method="post">

						<input type="text" name="your-name" value="" />
						<button type="submit">
							Subscribe <i class="fa fa-envelope"></i>
						</button>

					<!-- END .newsletter-form -->
					</form>

				<!-- END .one-fourth -->
				</div>

				<!-- BEGIN .one-fourth -->
				<div class="one-fourth">

					<h5>Contact Details</h5>
					<div class="title-block6"></div>
					<ul class="contact-widget">
						<li class="cw-address">700 5th Avenue, New York City, United States</li>
						<li class="cw-phone">1800-1111-2222<span>Mon - Fri, 9.00am until 6.30pm</span></li>
						<li class="cw-cell">booking@example.com<span>We reply within 24 hrs</span></li>
					</ul>

				<!-- END .one-fourth -->
				</div>

				<div class="clearboth"></div>

				<!-- END .footer-bottom -->
				</div>

				<!-- BEGIN .footer-bottom -->
				<div class="footer-bottom">

					<div class="footer-social-icons-wrapper">
						<a href="#"><i class="fa fa-facebook"></i></a>
						<a href="#"><i class="fa fa-twitter"></i></a>
						<a href="#"><i class="fa fa-instagram"></i></a>
						<a href="#"><i class="fa fa-youtube-play"></i></a>
						<a href="#"><i class="fa fa-pinterest"></i></a>
						<a href="#"><i class="fa fa-tumblr"></i></a>
					</div>

					<p class="footer-message">&copy; 2017 Chauffeur. All Rights Reserved</p>

			<!-- END .footer-inner -->
			</div>

		<!-- END .footer -->	
		</footer>
		
	<!-- END .outer-wrapper -->
	</div>
	
	<!-- JavaScript -->
	<script src="<?php echo base_url();?>assets-front/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets-front/js/jquery.themepunch.tools.min838f.js?rev=5.0"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets-front/js/jquery.themepunch.revolution.min838f.js?rev=5.0"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets-front/js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets-front/js/jquery.prettyPhoto.js"></script>
	
	<!-- Only required for local server -->
	<script type="text/javascript" src="<?php echo base_url();?>assets-front/js/extensions/revolution.extension.video.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets-front/js/extensions/revolution.extension.slideanims.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets-front/js/extensions/revolution.extension.layeranimation.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets-front/js/extensions/revolution.extension.navigation.min.js"></script>
	<!-- Only required for local server -->
	
	<script type="text/javascript" src="<?php echo base_url();?>assets-front/js/scripts.js"></script>
	
<!-- END body -->
</body>

<!-- Mirrored from themes.quitenicestuff2.com/chauffeur/header-1/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 26 Apr 2018 07:18:57 GMT -->
</html>