
<div id="page-header">
	<h1>About Us</h1>
	<div class="title-block3"></div>
	<p><a href="<?php echo base_url('client/homecontroller');?>">Home</a><i class="fa fa-angle-right"></i>About Us</p>
</div>

		<!-- BEGIN .content-wrapper-outer -->
<div class="content-wrapper-outer clearfix">
	
	<!-- BEGIN .main-content -->
	<div class="main-content main-content-full">
		
		<!-- BEGIN .clearfix -->
		<div class="clearfix">
			
			<!-- BEGIN .qns-one-half -->
			<div class="qns-one-half">
				
				<h4>Our Mission</h4>
				<div class="title-block7"></div>
				
				<p>Aenean et ultrices neque. Proin pretium, felis in maximus consectetur, sapien est lacinia leo, vel consectetur velit dui at nisi. Vestibulum non ante metus. Aliquam auctor lacus pretium, accumsan libero in, aliquam lacus.</p>
				
				<ul class="border-list">
					<li>Cras efficitur iaculis vulputate curabitur eros dolor</li>
					<li>Aliquam in est sed dictum facilisis urna et commodo</li>
					<li>Suspendisse ex arcu, consequat ac blandit</li>
					<li>Morbi imperdiet ornare dui maecenas ultrices eget est</li>
					<li>Proin at nisi sit amet nulla iaculis blandit</li>
				</ul>
				
			<!-- END .qns-one-half -->
			</div>
			
			<!-- BEGIN .qns-one-half -->
			<div class="qns-one-half qns-last">
				
				<h4>How We Work</h4>
				<div class="title-block7"></div>
			
				<div class="video-wrapper video-wrapper-page">

					<div class="video-play">
						<a href="https://www.youtube.com/watch?v=Uv5ZRiAreHA" data-gal="prettyPhoto"><i class="fa fa-play"></i></a>
					</div>

				</div>
			
			<!-- END .qns-one-half -->
			</div>
		
		<!-- END .clearfix -->
		</div>
		
		<ul class="link-blocks clearfix">
			<li><h3><a href="#" class="link-block-3"><span class="link-text">Our Services</span><span class="link-arrow fa fa-angle-right"></span></a></h3></li>
			<li><h3><a href="#" class="link-block-3"><span class="link-text">Photo Gallery</span><span class="link-arrow fa fa-angle-right"></span></a></h3></li>
			<li><h3><a href="#" class="link-block-3"><span class="link-text">Book Now</span><span class="link-arrow fa fa-angle-right"></span></a></h3></li>
		</ul>
		
		<!-- BEGIN .clearfix -->
		<div class="clearfix">
			
			<!-- BEGIN .qns-one-half -->
			<div class="qns-one-half content-wrapper">
				
				<h4>Testimonials</h4>
				<div class="title-block7"></div>
				
				<!-- BEGIN .testimonial-wrapper-outer -->
				<div class="testimonial-wrapper-outer">

					<!-- BEGIN .testimonial-list-wrapper -->
					<div class="testimonial-list-wrapper owl-carousel3">

						<!-- BEGIN .testimonial-wrapper -->
						<div class="testimonial-wrapper">
							<p><span class="qns-open-quote">“</span>These guys are so reliable, I can always count on them to get me to my business meetings on time regardless of the traffic, weather or time of day! Highly Recommend!<span class="qns-close-quote">”</span></p>
							<div class="testimonial-image"><img src="<?php echo base_url();?>assets-front/images/image17.jpg" alt="" /></div>
							<div class="testimonial-author"><p>Mike Jones - BMW Grand Sedan</p></div>
						<!-- END .testimonial-wrapper -->
						</div>

						<!-- BEGIN .testimonial-wrapper -->
						<div class="testimonial-wrapper">
							<p><span class="qns-open-quote">“</span>Fast and professional are the best words to describe the Chauffeur team, they got me to my business meeting on time and in style, looking forward to next time!<span class="qns-close-quote">”</span></p>
							<div class="testimonial-image"><img src="<?php echo base_url();?>assets-front/images/image23.jpg" alt="" /></div>
							<div class="testimonial-author"><p>Peter Lord - Cadillac Escalade</p></div>
						<!-- END .testimonial-wrapper -->
						</div>

						<!-- BEGIN .testimonial-wrapper -->
						<div class="testimonial-wrapper">
							<p><span class="qns-open-quote">“</span>Me and my friends had a great driving around town in the Ford Party Bus drinking champagne and being treated fantastically by the driver, many thanks!<span class="qns-close-quote">”</span></p>
							<div class="testimonial-image"><img src="<?php echo base_url();?>assets-front/images/image24.jpg" alt="" /></div>
							<div class="testimonial-author"><p>Tony Webber - Ford Party Bus</p></div>
						<!-- END .testimonial-wrapper -->
						</div>

					<!-- END .testimonial-list-wrapper -->
					</div>

				<!-- END .testimonial-wrapper-outer -->
				</div>
				
			<!-- END .qns-one-half -->
			</div>
			
			<!-- BEGIN .qns-one-half -->
			<div class="qns-one-half qns-last">
				
				<h4>Our Company</h4>
				<div class="title-block7"></div>
			
				<p>Mauris placerat placerat ligula at scelerisque. Sed accumsan purus non turpis commodo fermentum. Curabitur efficitur faucibus blandit. Sed id sem ullamcorper, interdum metus hendrerit, mollis risus. Phasellus at ligula gravida, auctor ipsum non, posuere diam. Nam ac rutrum orci. Donec sed lorem tempor, dictum leo id, vulputate ante. Sed non maximus lacus, vel iaculis enim. Phasellus id diam quis odio egestas molestie sed placerat purus. Vivamus vel maximus diam.</p>

				<p>Etiam ornare mollis ex. Nam eget quam lectus. Morbi consequat risus tortor, viverra vestibulum sapien venenatis vitae. Aenean sagittis mauris eget imperdiet maximus. Nunc pulvinar urna justo, eu pretium diam accumsan varius. Donec quis tellus tincidunt, semper nisi vitae, facilisis turpis. Nullam blandit feugiat convallis. Fusce neque ex, mattis vitae lacus nec, posuere vehicula orci. Morbi ut</p>
				
			
			<!-- END .qns-one-half -->
			</div>
		
		<!-- END .clearfix -->
		</div>
		
		<hr class="space3" />
		
	<!-- END .main-content -->
	</div>

<!-- END .content-wrapper-outer -->
</div>
	