<div id="page-header">
    <h1> Login</h1>
    <div class="title-block3"></div>
    <p><a href="<?php echo base_url('client/homecontroller');?>">Home</a><i class="fa fa-angle-right"></i>User Login</p>
</div>
<!-- BEGIN .content-wrapper-outer -->
<div class="content-wrapper-outer clearfix">
    <!-- BEGIN .main-content -->
    <div class="main-content main-content-full">
        <!-- BEGIN .clearfix -->
        <div class="clearfix">
			<!-- BEGIN .qns-one-half -->
			<div class="qns-one qns-last">
                <div class="page-header">
                        <h1>Login</h1>
                        <div class="title-block7"></div>
                </div>
                <form action="#" class="" method="post">
                    <label>Email <span>*</span></label>
                    <input type="text" name="email" value="" />
                    
                    <label>Password <span>*</span></label>
                    <input type="password" name="password" value="" />

                    <button type="submit" class="btn btn-xl">
                        Login
                    </button>
                </form>
			</div>
		</div>
	</div>
</div>
<br><br>