<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1 class="text-capitalize">
  <?php echo $this->uri->segment(2);?> <?php echo $this->uri->segment(3);?>
    <small>Add <?php echo $this->uri->segment(2);?> details here</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Examples</a></li>
    <li class="active">Blank page</li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
<div class="box box-primary">
        <div class="box-header with-border">
        <?php $this->load->view('admin/alerts'); ?>
          <h3 class="box-title text-capitalize"><?php echo $this->uri->segment(2);?> <?php echo $this->uri->segment(3);?></h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->

       
        <?php echo form_open(); ?>
         <div class="box-body">
            <div class="form-group <?php echo form_error('name') ? 'has-error' : '';?>">
              <label for="name">Name</label>
              <input type="text" name="name" class="form-control" id="name" placeholder="Enter the state name" value="<?php echo set_value('email');?>">
              <?php echo form_error('name');?>
            </div>
            

          <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
