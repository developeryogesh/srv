<link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
  <h1 class="text-capitalize">
  <?php echo $this->uri->segment(2);?> <?php echo $this->uri->segment(3);?>
    <small>Add <?php echo $this->uri->segment(2);?> details here</small>
  </h1>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    
    <li class="active">Blank page</li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
<div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title text-capitalize"><?php echo $this->uri->segment(2); ?> Details</h3>
        </div>
        <div class="box-body">
        <!-- /.box-header -->
        <!-- form start -->
        <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>name</th>
                  <th>create data</th>
                  <th>update date</th>
                  <th>status</th>
                  <th>action</th>
                </tr>
                </thead>
                <tbody>
              <?php 
              foreach($stateDtail as $stateDtails)
              {
              ?>
                <tr>
                 <td><?php echo $stateDtails->name ;?></td>
                 <td><?php echo $stateDtails->created_at ;?></td>
                 <td><?php echo $stateDtails->updated_at ;?></td>
                 <td><?php echo $stateDtails->status ;?></td>
                <td><a class="btn btn-danger" href="<?php echo base_url('usercontroller/delete/'.$stateDtails->id)?>"><span class="glyphicon glyphicon-trash"></span></a></td>
                
                  
                </tr>
                <?php
              }
                ?>
               
                </tbody>
                <tfoot>
                <tr>
                  <th>name</th>
                  <th>create date</th>
                  <th>update date</th>
                  <th>status</th>
                  
                </tr>
                </tfoot>
                
              </table>
</div>
            </div>
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- DataTables -->
<script src="<?php echo base_url('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js'); ?>"></script>
<script>
$(document).ready(function(){
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
});
</script>