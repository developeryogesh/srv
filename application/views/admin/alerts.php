<?php
   if($this->session->flashdata('success'))
    {
?>
    <div class="alert alert-success alert-dismissible">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>success!</strong> <?php echo $this->session->flashdata('success');?>
    </div>
<?php
  }
?>