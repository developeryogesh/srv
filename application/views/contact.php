<div id="page-header">
	<h1>Contact Us</h1>
	<div class="title-block3"></div>
	<p><a href="<?php echo base_url('client/homecontroller');?>">Home</a><i class="fa fa-angle-right"></i>Contact Us</p>
</div>

<!-- BEGIN .content-wrapper-outer -->
<div class="content-wrapper-outer clearfix">
	
	<!-- BEGIN .main-content -->
	<div class="main-content main-content-full">
		
		<!-- BEGIN .clearfix -->
		<div class="clearfix">
			
			<!-- BEGIN .qns-one-half -->
			<div class="qns-one-half">
				
				<!-- BEGIN .contact-form-1 -->
				<form action="#" class="contact-form-1" method="post">

					<label>Name <span>*</span></label>
					<input type="text" name="accommodation-keywords" value="" />

					<label>Email <span>*</span></label>
					<input type="text" name="accommodation-keywords" value="" />

					<label>Subject</label>
					<input type="text" name="accommodation-keywords" value="" />

					<label>Message <span>*</span></label>
					<textarea cols="10" rows="9"></textarea>	

					<button type="submit">
						Send <i class="fa fa-envelope"></i>
					</button>

				<!-- END .contact-form-1 -->
				</form>
				
			<!-- END .qns-one-half -->
			</div>
			
			<!-- BEGIN .qns-one-half -->
			<div class="qns-one-half qns-last">
				
				<h4>Get In Touch</h4>
				<div class="title-block7"></div>
			
				<p>Duis commodo ipsum quis ante venenatis rhoncus. Vivamus imperdiet felis ac facilisis hendrerit. Nulla eu elementum ex, ut pulvinar est. Nam aliquet et tortor sed aliquet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Integer sit amet aliquet leo sed aliquam ex.</p>
			
				<h4>Contact Details</h4>
				<div class="title-block7"></div>
					
				<ul class="contact-details-list">
					<li class="cdw-address clearfix">700 5th Avenue, New York City, United States</li>
					<li class="cdw-phone clearfix">1800-1111-2222</li>
					<li class="cdw-email clearfix">booking@example.com</li>
				</ul>
				
				<h4>Social Media</h4>
				<div class="title-block7"></div>
				
				<ul class="social-links clearfix">
					<li><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>
					<li><a href="#" target="_blank"><i class="fa fa-instagram"></i></a></li>
					<li><a href="#" target="_blank"><i class="fa fa-pinterest"></i></a></li>
					<li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
					<li><a href="#" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
				</ul>
				
			<!-- END .qns-one-half -->
			</div>
		
		<!-- END .clearfix -->
		</div>
		
		<hr class="space3" />
		
		<!-- BEGIN #google-map -->
		<div id="google-map" style="height: 385px;margin: 0 0 70px 0;"></div>

		<script type="text/javascript">

			function initialize() {

				// Set Location
				var myLatlng = new google.maps.LatLng(40.703316,-73.988145);

				// Set Style
				var styles = [
					{
						stylers: [
						{ hue: "#e8848e" },
						{ saturation: -50 }
						]
					},{
						featureType: "road",
						elementType: "geometry",
						stylers: [
						{ lightness: 100 },
						{ visibility: "simplified" }
						]
					},{
						featureType: "road",
						elementType: "labels",
						stylers: [
						{ visibility: "off" }
						]
					}
					];

				// Set Map Options
				var mapOptions = {
					mapTypeControlOptions: {
						mapTypeIds: ['Styled']
					},
					center: myLatlng,
					zoom: 14,
					mapTypeId: 'Styled',
					scrollwheel: false,
					scaleControl: false,
					disableDefaultUI: false
				};

				// Build Map
				var map = new google.maps.Map(document.getElementById('google-map'), mapOptions);
				var styledMapType = new google.maps.StyledMapType(styles, { name: 'Styled' });
				map.mapTypes.set('Styled', styledMapType);

				// Set Map Marker
				var contentString = 'Our Retirement Home';
				var infowindow = new google.maps.InfoWindow({
					content: contentString
				});
				var marker = new google.maps.Marker({
					position: myLatlng,
					map: map,
					title: 'Map Marker 1',
					icon: {
							path: fontawesome.markers.MAP_MARKER,
							scale: 0.8,
							strokeWeight: 0,
							strokeColor: 'black',
							strokeOpacity: 1,
							fillColor: '#cc4452',
							fillOpacity: 1,
						},
				});

				// Display Map Marker
				google.maps.event.addListener(marker, 'click', function() {
					infowindow.open(map,marker);
				});

			}

			// Display Map
			google.maps.event.addDomListener(window, 'load', initialize);

		</script>
		
		<hr class="space3" />
		
	<!-- END .main-content -->
	</div>

<!-- END .content-wrapper-outer -->
</div>