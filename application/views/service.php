<?php include('header.php');?>
<div id="page-header">
    <h1>Booking Step 2</h1>
    <div class="title-block3"></div>
    <p><a href="#">Home</a><i class="fa fa-angle-right"></i>Booking Step 2</p>
</div>
		
		<!-- BEGIN .content-wrapper-outer -->
<div class="content-wrapper-outer clearfix" >
    
    <!-- BEGIN .main-content -->
    <div class="main-content main-content-full " style="width:100%">
        
        <!-- BEGIN .booking-step-wrapper -->
        
        
        <!-- BEGIN .clearfix -->
        <div class="clearfix ">
        
            <!-- BEGIN .select-vehicle-wrapper -->
            <div class="select-vehicle-wrapper">
                <h4>Select Vehicle</h4>
                <div class="col-md-12 col-sm-12" >
                
                <div class="title-block7"></div>
            
                <div class="vehicle-section clearfix">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <p>Audi Grand Sedan <strong>Ac</strong></p>
                        </div>
                        <div class="col-md-4">
                            <p>50000</p>
                        </div>
                            <!-- <ul>
                            <li class="vehicle-bag-limit">2</li>
                            <li class="vehicle-passenger-limit">4</li>
                        </ul> -->
                        <img src="../assets/images/image66.jpg" alt="" />
                
                    </div>
                </div>
                <!-- BEGIN .vehicle-section -->
                <div class="vehicle-section clearfix">
                    
                    <p>Mercedes Sedan <strong>$130.90</strong></p>
                    <ul>
                        <li class="vehicle-bag-limit">2</li>
                        <li class="vehicle-passenger-limit">4</li>
                    </ul>
                    <img src="../assets/images/image67.jpg" alt="" />
                
                <!-- END .vehicle-section -->
                </div>
                
                </div>
            <!-- END .select-vehicle-wrapper -->
            </div>
            
            <!-- BEGIN .trip-details-wrapper -->
        
        <!-- END .clearfix -->
        </div>
        
    <!-- END .main-content -->
    </div>

<!-- END .content-wrapper-outer -->
</div>
		<?php include('footer.php');?>