<?php
    class AboutusController extends CI_Controller
    {
        public function __construct()
        {
            parent::__construct();
        }
        public function index()
        {
            $this->load->view('header');
            $this->load->view('about_us');
            $this->load->view('footer');
            
        }
        public function contact()
        {
            $this->load->view('header');
            $this->load->view('contact');
            $this->load->view('footer');
            
        }
    }
?>