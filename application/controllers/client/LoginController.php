<?php 
    class LoginController extends CI_Controller
    {
        public function __construct()
        {
            parent::__construct();
        }
        public function index()
        {
            $this->load->view('header');
            $this->load->view('login');
            $this->load->view('footer');
            
        }
        public function enquiry()
        {
            $this->load->view('header');
            $this->load->view('enquiry');
            $this->load->view('footer');
            
        }
    }
?>