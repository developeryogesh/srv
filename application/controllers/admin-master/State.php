<?php

/**
* 
*/
class State extends CI_controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('admin/CrudModel');

	}
	public function index()
	{
		
			$this->load->view('admin/header');
			$this->load->view('admin/state/index');
			$this->load->view('admin/footer');
		
	}
	public function create()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<span class="help-block">','<span>');
		$this->form_validation->set_rules('name','state name','required');
		if($this->form_validation->run() == false)
		{
			$this->load->view('admin/header');
			$this->load->view('admin/state/create');
			$this->load->view('admin/footer');
		}
		else
		{
			$stateDetail = 
			[
				'name' => $this->input->post('name')
			];
			$res = $this->CrudModel->insert('state',$stateDetail);
			
			if($res)
			{
				$this->session->set_flashdata('success','state insert Successfully !!!');
				 redirect('admin-master/state/create');
			}
			else
			{
		 redirect('state');
				echo "error";
			}
		}

		
	}
	public function delete()
	{
		
	}
	
	public function view()
	{
		$stateDtails['stateDtail']= $this->CrudModel->select_where('state');
		//echo $this->db->last_query();

			
			$this->load->view('admin/header');
			$this->load->view('admin/state/index',$stateDtails);
			$this->load->view('admin/footer');
	}
	
	
}

?>
